## Team 8 - Improved Product Recommendations

### Project Overview
----------------------------------

A brief description of 
* What problem did the team try to solve
  1. Based on user’s browsing habits on different websites, we were asked to make a recommendation engine.

  Taking the example of buying gaming laptops, we made our model.
* What is the proposed solution
  1. CXM Model based on innovative tracking methodology to make state-of-the-art recommendations
  2. User behaviour and data tracking
  3. Data saving and retrieval
  4. Product recommendations


### Solution Description
----------------------------------
A new customer experience management model where dell and some of the popular e-commerce sites agree to a common platform to exchange data on the user's machine and recommending products based on the data gathered on all the signed up websites. Our model is easily deployable,cost-efficient and with zero database management. We used localStorage coupled with JSONP to save and share data directly on the user's machine. When the user lands on dell.com page, all the data collected and saved by the signed up websites is requested and is sent to the backgound server for product recommendations.

#### Architecture Diagram

![](./documentation/architecture.png)

#### Technical Description

An overview of 
* We used the following technologies to develop our solution
		-HTML5
		-CSS3
		-Django 2.6
		-Python 3.7
		-Javascript 1.8.5
		-SQLite 3.30.1
		-Bootstrap Framework v4.3.1

* Setup/Installations required to run the solution
  - The Application Code folder has a total of 4 folders with 4 different websites. dellProject is a dynamic website to be hosted using Django. All the other websites are static and are in github format. Pandas,keras library is required to run the backend engine of dell website.
* Instructions to run the submitted code: All the links are in relative form and may not work as expected when run on another machine.

### Team Members
----------------------------------
1. T. Anil Kumar, 17511, anilkumar170511@mechyd.ac.in
		-Dynamic Dell Website, Database Management, Frontend-Backend connection, Search Engine

2. Teja Nagubandi, 17527, sriteja170527@mechyd.ac.in
		-2 e-commerce websites, user data retrieval, Javascript events

3. Dheeraj Goli, 17512, dheeraj170512@mechyd.ac.in
		-TfifdVectorizer, Count Vectorizer, KNN

4. Preetham Raj, 17523, preetham170523@mechyd.ac.in
		-Data Collection, Custom-built algorithms with Cosine, Jaccardian, Pearson Similarities,NLP, Case Studies



